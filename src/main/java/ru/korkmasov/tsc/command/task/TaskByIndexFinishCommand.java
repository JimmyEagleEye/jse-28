package ru.korkmasov.tsc.command.task;

import ru.korkmasov.tsc.util.TerminalUtil;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class TaskByIndexFinishCommand extends AbstractTaskCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "task-finish-by-index";
    }

    @NotNull
    @Override
    public String description() {
        return "Finish task by index";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[START TASK]");
        System.out.println("ENTER INDEX:");
        serviceLocator.getTaskService().startTaskByIndex(userId, TerminalUtil.nextNumber() - 1);
    }

}
