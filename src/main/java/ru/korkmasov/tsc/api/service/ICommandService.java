package ru.korkmasov.tsc.api.service;

import ru.korkmasov.tsc.command.AbstractCommand;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;

public interface ICommandService {

    @Nullable
    Collection<AbstractCommand> getCommands();

    @Nullable
    Collection<AbstractCommand> getArguments();

    @Nullable
    AbstractCommand getCommandByName(String name);

    @Nullable
    AbstractCommand getCommandByArg(String arg);

    @Nullable
    Collection<String> getListArgumentName();

    @Nullable
    Collection<String> getListCommandName();

    void add(@NotNull AbstractCommand command);

}
